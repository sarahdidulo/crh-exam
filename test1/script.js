
let element = document.querySelector(".navlink");
let menu = document.querySelector("#menu");
let navlinksmall = document.querySelector(".navlink-smallscrn");
let navcontent = document.querySelector(".nav-content");
let nav = document.querySelector("nav");


window.addEventListener('resize', function(event){
	if(window.screen.width <= 768){
		element.classList.add("displaynone");
		menu.classList.remove("displaynone");
	} else {
		element.classList.remove("displaynone");
		menu.classList.add("displaynone");
		navlinksmall.classList.add("displaynone");
		navlinksmall.classList.remove("display-block");
	}
});

menu.onclick = function(){
	navlinksmall.classList.toggle("displaynone");
	navlinksmall.classList.toggle("display-block");
}
