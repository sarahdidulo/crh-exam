
let btn = document.getElementById("btn");
let btnHover = document.getElementById("btn-hover");

btnHover.style.display = "none";

btn.addEventListener("mouseover", function(){
	btnHover.style.display = "block";
	btn.style.display = "none";
})

btnHover.addEventListener("mouseout", function(){
	btn.style.display = "block";
	btnHover.style.display = "none";
})


